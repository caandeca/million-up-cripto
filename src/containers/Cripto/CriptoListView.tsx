import React, { useEffect, memo } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { getCoins, getAllCoins } from '../../store/actions/criptoActions';
import { LoadingCoins, orderAscBy, orderDescBy } from '../../store/reducers/CriptoSlice';
import CriptoList from '../../components/Cripto/CriptoList/CriptoList';
import Loading from '../../basics/Loading/Loading';
import CriptoHeader from '../../components/Cripto/CriptoHeader/CriptoHeader';
import Notification from '../../basics/Notification/Notification';
import Translate from '../../language/Lenguage';

const CriptoListView: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch();

  const { criptoList, totalCripto, loading, totalVirtualCripto, isFinishedLoading } =
    useAppSelector((state) => state.cripto);

  useEffect(() => {
    if (criptoList.length == 0) {
      dispatch(LoadingCoins());
      dispatch(getCoins({ startIndex: 0, stopIndex: 100 })).then(() => dispatch(getAllCoins()));
    }
  }, []);

  const handleSortBy = (key: string, asc: boolean) => {
    if (isFinishedLoading) {
      if (asc) {
        dispatch(orderAscBy(key));
      } else {
        dispatch(orderDescBy(key));
      }
    }
  };

  if (loading) return <Loading />;
  return (
    <>
      {!isFinishedLoading && (
        <Notification
          title={<Translate message="loadAllData" />}
          text={<Translate message="messageLoadAllData" />}
        />
      )}
      <CriptoHeader handleSortBy={handleSortBy} />
      <CriptoList
        totalVirtualCripto={totalVirtualCripto}
        totalCripto={totalCripto}
        criptoList={criptoList}
        loading={loading}
      />
    </>
  );
};

export default memo(CriptoListView);
