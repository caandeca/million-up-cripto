import React from 'react';
import Cripto from '../../components/Cripto/Cripto/Cripto';

const CriptoView: React.FC = (): JSX.Element => {
  return (
    <Cripto
      id=""
      symbol=""
      name=""
      nameid=""
      rank={0}
      price_usd=""
      percent_change_24h=""
      percent_change_1h=""
      percent_change_7d=""
      price_btc=""
      market_cap_usd=""
      volume24={0}
      volume24a={0}
      csupply=""
      tsupply=""
      msupply=""
    />
  );
};

export default CriptoView;
