import React from 'react';
import PageNotFound from '../../components/Error/PageNotFound/PageNotFound';

const PageNotFoundView: React.FC = (): JSX.Element => {
  return <PageNotFound />;
};

export default PageNotFoundView;
