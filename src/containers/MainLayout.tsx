import React from 'react';
import { useNavigate } from 'react-router-dom';
import Main from '../components/Main/Main';

const MainLayout: React.FC = (): JSX.Element => {
  return <Main />;
};

export default MainLayout;
