import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { Translations } from '../../language/Translations';
import { changeLocate } from '../../store/reducers/GeneralSlice';
import { searchCripto } from '../../store/reducers/CriptoSlice';
import Navbar from '../../components/Navbar/Navbar';

const NavbarView: React.FC = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { locate } = useAppSelector((state) => state.general);
  const { isFinishedLoading } = useAppSelector((state) => state.cripto);
  const handleChangeLocate = (locateChange: keyof typeof Translations) => {
    if (locate != locateChange) dispatch(changeLocate(locateChange));
  };

  const handleNavigate = (path: string) => {
    navigate(path);
  };

  const handleChange = (value: string) => {
    dispatch(searchCripto(value));
  };

  return (
    <Navbar
      changeLocate={handleChangeLocate}
      handleChange={handleChange}
      navigate={handleNavigate}
      locate={locate}
      isFinishedLoading={isFinishedLoading || false}
    />
  );
};

export default NavbarView;
