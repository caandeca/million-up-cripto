import React from 'react';
import IPageNotFound from './PageNotFoundInterface';

const PageNotFound: React.FC<IPageNotFound> = ({}: IPageNotFound): JSX.Element => {
  return <div>PageNotFound</div>;
};

export default PageNotFound;
