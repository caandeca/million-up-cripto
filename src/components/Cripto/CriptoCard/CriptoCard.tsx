import React, { useState, memo } from 'react';
import { numberWithCommas } from '../../../utils/parseData';
import PercentData from '../../../basics/PercentData/PercentData';
import Translate from '../../../language/Lenguage';
import styleCard from './CriptoCard.module.scss';
import ICriptoCard from './CriptoCardInterface';
import CriptoTitle from '../../../basics/CardTitle/CriptoTitle';

const CriptoCard: React.FC<ICriptoCard> = ({ style, data, index }: ICriptoCard): JSX.Element => {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <div style={style} className="flex_center">
      <div className={styleCard.container}>
        <CriptoTitle />
        <div className="grid_cripto">
          <div className={styleCard.ranking}>
            <div className="">{data.rank}</div>
          </div>
          <div className={styleCard.name_img}>
            <img
              style={{ display: isLoading ? 'none' : 'block' }}
              src={`https://www.coinlore.com/img/25x25/${data.nameid}.png`}
              className=""
              onLoad={() => setIsLoading(false)}
            />
            <div className={styleCard.name}>{data.name}</div>
          </div>
          <div className="">{'$' + numberWithCommas(data.price_usd)}</div>
          <div className="">{'$' + numberWithCommas(data.volume24.toFixed(0).toString())}</div>
          <div className="">{'$' + numberWithCommas(parseInt(data.market_cap_usd).toString())}</div>
          <PercentData text={data.percent_change_1h} />
          <PercentData text={data.percent_change_24h} />
          <PercentData text={data.percent_change_7d} />
        </div>
      </div>
    </div>
  );
};

export default memo(CriptoCard);
