import ICripto from '../Cripto/CriptoInterface';

export default interface ICard {
  data: ICripto;
  style: object;
  index: number;
}
