export default interface ICriptoHeader {
  handleSortBy(key: string, asc: boolean): void;
}
