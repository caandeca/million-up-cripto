import React from 'react';
import ICriptoHeader from './CriptoHeaderInterface';
import style from './CriptoHeader.module.scss';
import CriptoTitle from '../../../basics/CardTitle/CriptoTitle';
import Arrows from '../../../basics/Arrows/Arrows';

const CriptoHeader: React.FC<ICriptoHeader> = ({ handleSortBy }: ICriptoHeader): JSX.Element => {
  return (
    <div className={style.scrollOff}>
      <div className={style.container}>
        <CriptoTitle />
        <div className={style.content_filters}>
          <Arrows handleSortBy={handleSortBy} keyFilter="rank" />
          <Arrows handleSortBy={handleSortBy} keyFilter="name" />
          <Arrows handleSortBy={handleSortBy} keyFilter="price_usd" />
          <Arrows handleSortBy={handleSortBy} keyFilter="volume24" />
          <Arrows handleSortBy={handleSortBy} keyFilter="market_cap_usd" />
          <Arrows handleSortBy={handleSortBy} keyFilter="percent_change_1h" />
          <Arrows handleSortBy={handleSortBy} keyFilter="percent_change_24h" />
          <Arrows handleSortBy={handleSortBy} keyFilter="percent_change_7d" />
        </div>
      </div>
    </div>
  );
};

export default CriptoHeader;
