import React from 'react';
import ICriptoList from './CriptoListInterface';
import { InfiniteLoader, List } from 'react-virtualized';
import CriptoCard from '../CriptoCard/CriptoCard';
interface Ielement {
  key: string;
  index: number;
  style: object;
}

const CriptoList: React.FC<ICriptoList> = ({
  criptoList,
  totalCripto,
  totalVirtualCripto
}: ICriptoList): JSX.Element => {
  const rowRenderer = ({ key, index, style }: Ielement) => {
    return <CriptoCard data={criptoList[index]} style={style} key={key} index={index} />;
  };
  return (
    <List
      height={800}
      rowCount={totalVirtualCripto || 0}
      rowHeight={80}
      rowRenderer={rowRenderer}
      width={500}
    />
  );
};

export default CriptoList;
