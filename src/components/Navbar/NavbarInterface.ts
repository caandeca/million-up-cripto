export default interface INavbar {
  changeLocate(locate: string): void;
  navigate(path: string): void;
  handleChange(value: string): void;
  locate: string;
  isFinishedLoading: boolean;
}
