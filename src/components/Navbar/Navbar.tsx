import React from 'react';
import INavbar from './NavbarInterface';
import style from './Navbar.module.scss';
import logo from '../../assets/img/logo.png';
import InputSearch from '../../basics/InputSearch/InputSearch';

const Navbar: React.FC<INavbar> = ({
  changeLocate,
  navigate,
  handleChange,
  isFinishedLoading
}: INavbar): JSX.Element => {
  return (
    <div className={style.container_navbar}>
      <div className={style.container}>
        <InputSearch disabled={isFinishedLoading} handleChange={handleChange} />
        <img
          className={style.logo}
          onClick={() => {
            navigate('/');
          }}
          src={logo}
          alt="logo"
        />
        {/*  <div className="row">
            <div
              onClick={() => {
                navigate('/');
              }}
              className={style.item}
            >
              <Translate message="crypto" />
            </div>
            <div
              onClick={() => {
                navigate('/Exchanges');
              }}
              className={style.item}
            >
              <Translate message="exchange" />
            </div>
          </div> */}
        <div className={style.locate}>
          <div
            onClick={() => {
              changeLocate('es');
            }}
            className={style.item_locate}
          >
            Es
          </div>
          <div
            onClick={() => {
              changeLocate('en');
            }}
            className={style.item_locate}
          >
            En
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
