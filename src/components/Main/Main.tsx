import React from 'react';
import { Outlet } from 'react-router-dom';
import IMain from './MainInterface';
import Navbar from '../../containers/Navbar/NavbarView';

const Main: React.FC<IMain> = ({}: IMain): JSX.Element => {
  return (
    <div>
      <Navbar />
      <div className="container">
        <Outlet />
      </div>
    </div>
  );
};

export default Main;
