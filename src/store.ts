import { configureStore } from '@reduxjs/toolkit';
import criptoReducer from './store/reducers/CriptoSlice';
import GeneralReducer from './store/reducers/GeneralSlice';

export const store = configureStore({
  reducer: {
    cripto: criptoReducer,
    general: GeneralReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: { warnAfter: 128 },
      serializableCheck: { warnAfter: 128 }
    }),
  devTools: false
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
