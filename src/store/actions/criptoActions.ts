import http from '../../http-common';
import { createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from '../../store';
export interface IndexRange {
  startIndex: number;
  stopIndex: number;
}

export const getCoins = createAsyncThunk(
  'coins/getCoins',
  async ({ startIndex, stopIndex }: IndexRange) => {
    try {
      const getAllCrypto = await http.get(`tickers/?start=${startIndex}&limit=${stopIndex}`);
      return getAllCrypto.data;
    } catch (error) {
      console.log('error renovations service: ', error);
      throw error;
    }
  }
);

export const getAllCoins = createAsyncThunk(
  'coins/getAllCoins',
  async (_, { getState, dispatch }) => {
    const {
      cripto: { totalCripto }
    } = getState() as RootState;
    const TotalFor = parseInt((totalCripto / 100).toFixed(0));
    for (let index = 1; index < TotalFor + 1; index++) {
      (function (i) {
        setTimeout(() => {
          dispatch(getCoins({ startIndex: index * 100, stopIndex: 100 }));
        }, i * 50);
      })(index);
    }
  }
);
