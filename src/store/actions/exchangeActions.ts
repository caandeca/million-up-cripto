import http from '../../http-common';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const getExchanges = createAsyncThunk('exchanges/allExchanges', async () => {
  try {
    const allExchanges = await http.get(`exchanges/`);
    return allExchanges.data;
  } catch (error) {
    console.log('error renovations service: ', error);
    throw error;
  }
});
