import { createSlice } from '@reduxjs/toolkit';
import { Translations } from '../../language/Translations';

export interface ICriptoState {
  locate: keyof typeof Translations;
}

const initialState: ICriptoState = {
  locate: 'es'
};

export const GeneralSlice = createSlice({
  name: 'cripto',
  initialState,
  reducers: {
    changeLocate: (state, { payload }: { payload: keyof typeof Translations }) => {
      state.locate = payload;
    }
  }
});

export const { changeLocate } = GeneralSlice.actions;

export default GeneralSlice.reducer;
