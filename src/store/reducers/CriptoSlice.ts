import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getCoins } from '../actions/criptoActions';
import { sortAsc, sortDesc } from '../../utils/orderData';
import ICripto from '../../components/Cripto/Cripto/CriptoInterface';
export interface ICriptoState {
  loading: boolean;
  totalCripto: number;
  totalVirtualCripto?: number;
  isFinishedLoading?: boolean;
  criptoList: Array<ICripto>;
  FiltercriptoList?: Array<ICripto>;
}

const initialState: ICriptoState = {
  loading: true,
  totalCripto: 0,
  totalVirtualCripto: 0,
  isFinishedLoading: false,
  criptoList: [],
  FiltercriptoList: []
};

export const criptoSlice = createSlice({
  name: 'cripto',
  initialState,
  reducers: {
    LoadingCoins: (state) => {
      state.loading = true;
    },
    FinishLoadingCoins: (state) => {
      state.loading = false;
    },
    orderAscBy: (state, action: PayloadAction<string>) => {
      let newCriptoList = state.criptoList;
      newCriptoList = sortAsc(newCriptoList, action.payload, action.payload == 'name');
      state.criptoList = newCriptoList;
    },
    orderDescBy: (state, action: PayloadAction<string>) => {
      let newCriptoList = state.criptoList;
      newCriptoList = sortDesc(newCriptoList, action.payload, action.payload == 'name');
      state.criptoList = newCriptoList;
    },
    searchCripto: (state, action: PayloadAction<string>) => {
      let newCriptoList = state.FiltercriptoList || [];
      newCriptoList = newCriptoList.filter((cripto) =>
        cripto.name.toUpperCase().includes(action.payload.toUpperCase())
      );
      state.criptoList = newCriptoList;
      state.totalVirtualCripto = newCriptoList.length;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCoins.pending, () => {})
      .addCase(getCoins.fulfilled, (state, action) => {
        let newTotal = state.totalVirtualCripto || 0;
        let criptoListState = [...(state.FiltercriptoList || []), ...action.payload.data];
        let loadingAllData = false;
        if (
          newTotal >= parseInt((state.totalCripto / 100).toFixed(0)) * 100 &&
          state.totalCripto != 0
        ) {
          loadingAllData = true;
          criptoListState = sortAsc(criptoListState, 'rank', false);
          newTotal = state.totalCripto;
        } else newTotal += 100;
        return {
          ...state,
          criptoList: criptoListState,
          FiltercriptoList: criptoListState,
          totalCripto: action.payload.info.coins_num,
          totalVirtualCripto: newTotal,
          loading: false,
          isFinishedLoading: loadingAllData
        };
      })
      .addCase(getCoins.rejected, () => {});
  }
});

export const { LoadingCoins, FinishLoadingCoins, orderAscBy, orderDescBy, searchCripto } =
  criptoSlice.actions;

export default criptoSlice.reducer;
