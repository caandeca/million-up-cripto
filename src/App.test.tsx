import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders a message', () => {
  const { container, getByText } = render(<App />);
  expect(getByText('CriptoList')).toBeTruthy();
});
