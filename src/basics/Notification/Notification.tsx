import React from 'react';
import INotification from './NotificationInterface';
import style from './Notification.module.scss';

const Notification: React.FC<INotification> = ({ title, text }: INotification): JSX.Element => {
  return (
    <div className={style.container}>
      <div className={style.title}>{title}</div>
      {text}
    </div>
  );
};

export default Notification;
