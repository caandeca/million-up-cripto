import { ReactElement } from 'react';

export default interface INotification {
  title: ReactElement<any, any>;
  text: ReactElement<any, any>;
}
