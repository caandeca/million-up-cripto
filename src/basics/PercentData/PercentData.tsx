import React from 'react';
import IPercentData from './PercentDataInterface';
import style from './PercentData.module.scss';

const PercentData: React.FC<IPercentData> = ({ text }: IPercentData): JSX.Element => {
  return <div className={parseInt(text) > 0 ? style.green : style.red}>{text + '%'}</div>;
};

export default PercentData;
