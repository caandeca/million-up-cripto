export default interface ICard {
  title: string;
  rank?: number;
  img: string;
  style: object;
  index: number;
  exchange?: boolean;
}
