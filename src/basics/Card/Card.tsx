import React, { useState, memo } from 'react';
import styleCard from './Card.module.scss';
import ICard from './CardInterface';

const Card: React.FC<ICard> = ({
  style,
  title,
  img,
  exchange,
  index,
  rank
}: ICard): JSX.Element => {
  const [isLoading, setIsLoading] = useState(true);

  return (
    <div style={style} className="flex_center">
      <div className={styleCard.container}>
        <div className="">{rank + ':' + title}</div>
        <img
          style={{ display: isLoading ? 'none' : 'block' }}
          src={`https://www.coinlore.com/img/${exchange ? 'exchanges/' : ''}25x25/${img}.png`}
          className=""
          onLoad={() => setIsLoading(false)}
        />
      </div>
    </div>
  );
};

export default memo(Card);
