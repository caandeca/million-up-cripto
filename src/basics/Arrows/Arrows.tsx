import React, { memo } from 'react';
import { ArrowIcon } from '../../assets/icons';
import style from './Arrows.module.scss';
import IArrows from './ArrowsInterface';

const Arrows: React.FC<IArrows> = ({ handleSortBy, keyFilter }: IArrows): JSX.Element => {
  return (
    <div className={style.container}>
      <ArrowIcon onClick={() => handleSortBy(keyFilter, true)} className={style.up_arrow} />
      <ArrowIcon onClick={() => handleSortBy(keyFilter, false)} className={style.down_arrow} />
    </div>
  );
};

export default memo(Arrows);
