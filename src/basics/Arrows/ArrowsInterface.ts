import ICriptoHeader from '../../components/Cripto/CriptoHeader/CriptoHeaderInterface';

export default interface ICard extends ICriptoHeader {
    keyFilter: string;
}
