export default interface IInputSearch {
  handleChange(value: string): void;
  disabled?: boolean;
}
