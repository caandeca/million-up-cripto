import React from 'react';
import { SearchIcon } from '../../assets/icons';
import Translate from '../../language/Lenguage';
import style from './InputSearch.module.scss';
import IInputSearch from './InputSearchInterface';

const InputSearch: React.FC<IInputSearch> = ({
  handleChange,
  disabled
}: IInputSearch): JSX.Element => {
  return (
    <div className={style.container}>
      <div className={style.search_text}>
        <Translate message="search" />
      </div>
      <div className="flex_center">
        <input
          disabled={!disabled}
          onChange={(e) => handleChange(e.target.value)}
          className={style.input}
          type="text"
        />
        <SearchIcon className={style.icon} />
      </div>
    </div>
  );
};

export default InputSearch;
