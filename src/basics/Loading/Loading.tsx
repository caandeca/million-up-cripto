import React from 'react';
import ILoading from './LoadingInterface';
import style from './Loading.module.scss';

const Loading: React.FC<ILoading> = ({}: ILoading): JSX.Element => {
  return (
    <div className={style.content}>
      <div className={style.loader}></div>
    </div>
  );
};

export default Loading;
