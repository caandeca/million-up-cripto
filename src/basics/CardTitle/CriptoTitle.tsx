import React from 'react';
import Translate from '../../language/Lenguage';

const CriptoTitle = () => {
  return (
    <div className="grid_cripto">
      <div className="small_text">
        <Translate message="rank" />
      </div>
      <div className="small_text">
        <Translate message="name" />
      </div>
      <div className="small_text">
        <Translate message="price" />
      </div>
      <div className="small_text">
        <Translate message="volume" />
      </div>
      <div className="small_text">
        <Translate message="marketCap" />
      </div>
      <div className="small_text">{'1h'}</div>
      <div className="small_text">{'24h'}</div>
      <div className="small_text">{'7d'}</div>
    </div>
  );
};

export default CriptoTitle;
