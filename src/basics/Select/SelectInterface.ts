export default interface ISelect {
  title: string;
  options: Array<any>;
}
