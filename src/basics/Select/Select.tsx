import React from 'react';
import { SearchIcon } from '../../assets/icons';
import Translate from '../../language/Lenguage';
import style from './Select.module.scss';
import ISelect from './SelectInterface';

const Select: React.FC<ISelect> = ({ title, options }: ISelect): JSX.Element => {
  return (
    <div className={style.container}>
      <div className={style.title}>{title}</div>
      <select id="selectbox" data-selected="">
        <option value="" selected disabled>
          <Translate message="name" />
        </option>
        <option value="1">Ocean Wall</option>
        <option value="2">Skate Park</option>
        <option value="3">Mountain View</option>
        <option value="4">Cityscape</option>
        <option value="5">Workshop</option>
      </select>
      {options.map((option, index) => {
        <option key={index} value={option.value}>
          Ocean Wall
        </option>;
      })}
    </div>
  );
};

export default Select;
