//in this object are all the keys that are part of the texts that are shown in all the pages
export default {
  crypto: 'Crypto',
  exchange: 'Exchange',
  rank: 'Rank',
  name: 'Nombre',
  price: 'Precio',
  percent_change: 'Porcentaje de cambio',
  volume: 'Volumen 24h',
  marketCap: 'Capitalización de mercado',
  search: 'Buscar',
  loadAllData: 'Cargando todos los datos',
  messageLoadAllData: 'Una vez se carguen todos los datos se habilitarán los filtros'
};
