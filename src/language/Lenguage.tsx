import React from 'react';
import { useAppSelector } from '../hooks';
import { Translations } from './Translations';

const defaultLocate = 'es';

interface ITranslate {
  message: keyof typeof Translations[typeof defaultLocate];
}

const Translate: React.FC<ITranslate> = ({ message }: ITranslate): JSX.Element => {
  const { locate } = useAppSelector((state) => state.general);
  return <>{Translations[locate][message]}</>;
};

export default Translate;
