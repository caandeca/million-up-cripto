//in this object are all the keys that are part of the texts that are shown in all the pages
export default {
  crypto: 'Crypto',
  exchange: 'Exchange',
  rank: 'Rank',
  name: 'Name',
  price: 'Price',
  percent_change: 'Percent change',
  volume: 'Volume 24h',
  marketCap: 'Market capitalization',
  search: 'Search',
  loadAllData: 'Loading all data',
  messageLoadAllData: 'Once all the data is loaded, the filters will be enabled'
};
