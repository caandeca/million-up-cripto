import spanish from './es';
import english from './en';

export const Translations = {
  es: spanish,
  en: english
};
