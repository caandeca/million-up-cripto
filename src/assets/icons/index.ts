import { ReactComponent as SearchIcon } from './search.svg';
import { ReactComponent as PlusIcon } from './plus.svg';
import { ReactComponent as ArrowIcon } from './arrow.svg';

export { SearchIcon, PlusIcon, ArrowIcon };
