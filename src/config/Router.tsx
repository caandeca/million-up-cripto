import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import MainLayout from '../containers/MainLayout';
import CriptoListView from '../containers/Cripto/CriptoListView';
import DetailView from '../containers/Cripto/CriptoView';
import PageNotFoundView from '../containers/Error/PageNotFoundView';

const Router: React.FC = (): JSX.Element => {
  const mainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: '*', element: <Navigate to="/404" /> },
      { path: '/', element: <CriptoListView /> },
      { path: '404', element: <PageNotFoundView /> },
      { path: '/detail/:id', element: <DetailView /> }
    ]
  };

  const routing = useRoutes([mainRoutes]);

  return <>{routing}</>;
};

export default Router;
