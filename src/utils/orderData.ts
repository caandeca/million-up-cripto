export const sortAsc: (vector: Array<any>, key: string, isString: boolean) => Array<any> = (
  vector: Array<any>,
  key: string,
  isString: boolean
) => {
  return vector.sort((a, b) => {
    if (!isString) return a[key] - b[key];
    if (a[key] > b[key]) {
      return 1;
    }
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  });
};

export const sortDesc: (vector: Array<any>, key: string, isString: boolean) => Array<any> = (
  vector: Array<any>,
  key: string,
  isString: boolean
) => {
  return vector.sort((a, b) => {
    if (!isString) return b[key] - a[key];
    if (a[key] > b[key]) {
      return -1;
    }
    if (a[key] < b[key]) {
      return 1;
    }
    return 0;
  });
};
